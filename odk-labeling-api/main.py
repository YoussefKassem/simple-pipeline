import os
import uvicorn
import cv2
import base64

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.websockets import WebSocket, WebSocketDisconnect

app = FastAPI(openapi_prefix=os.environ.get('API_PREFIX', '/'))

# set cors
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def index():
    return {
        "status": "succes",
        "message": "odk-labeling.stadswerken.amsterdam"
    }


@app.websocket("/annotate")
async def ws_stream(ws: WebSocket):
    await ws.accept()
    accepted = {"accepted": "Connection accepted"}
    await ws.send_json(accepted)

    try:
        while True:
            message = await ws.receive_json()

            try:
                if 'request' in message.keys():
                    photos = os.listdir('C:\\foto\\')
                    list = {'images': []}
                    for photo in photos:
                        with open('C:\\foto\\' + photo, "rb") as image_file:
                            encoded_image = base64.b64encode(image_file.read()).decode()
                        list['images'].append(encoded_image)
                    await ws.send_json(list)
                else:
                    await ws.send_text("Thanks for annotating")
            except ConnectionError:
                content = {"error": "MessageServer Not Available"}
                await ws.send_json(content)

    except WebSocketDisconnect:
        await ws.send_text("WebSocket /annotate [disconnect]")

if __name__ == "__main__":
    uvicorn.run(app,
                host="127.0.0.1",
                port=8090, workers=1)