import Vue from 'vue'
import VueRouter from 'vue-router'
import UserTypePage from './../components/UserTypePage'
import ModelsPage from './../components/ModelsPage'
import AnnotatePage from './../components/AnnotatePage'

Vue.use(VueRouter)

let router = new VueRouter({
    routes: [{
        path: '/',
        name: 'home-page',
        component: UserTypePage,
    },
    {
        path: '/models',
        name: 'models-page',
        component: ModelsPage,
    },
    {
        path: '/annotate',
        name: 'annotate-page',
        component: AnnotatePage,
    }]
})

export default router