import Vue from 'vue';
import App from './App';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import router from './router';
import messages from './locale/translations';
import VueI18n from 'vue-i18n';

export const eventBus = new Vue();

Vue.use(VueI18n)
Vue.use(Buefy)

const i18n = new VueI18n({
  locale: 'nl', // set locale
  fallbackLocale: 'nl',
  messages // set locale messages
})

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
