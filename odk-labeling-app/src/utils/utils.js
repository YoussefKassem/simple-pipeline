export const AllModels = {
    i18n: {
        messages: {
            en: {
                models: [{
                    name: "Garbage",
                    description: "Model to detect garbage on the street",
                    model: "GarbageDetection"
                },
                {
                    name: "Tree",
                    description: "Model to detect trees in public space",
                    model: "TreeDetection"
                }]
            },
            nl: {
                models: [{
                    name: "Afval",
                    description: "Model om afval op straat te herkennen",
                    model: "GarbageDetection"
                },
                {
                    name: "Bomen",
                    description: "Model om bomen in de openbare ruimte te detecteren",
                    model: "TreeDetection"
                }]
            }
        }
    }
}